scriptfile=${1:-'/usr/local/bin/pdf-generation'}
cookieval=${2:-NOCOOKIE}
deployment=${3:-bananastest.accelo.com}
objectparams=${4:-action=generate_invoice_pdf_html&id=50}

cookie={\"name\":\"AFFINITYLIVE\",\"value\":\"${cookieval}\",\"domain\":\"https://${deployment}\"}
url=https://${deployment}/?${objectparams}

echo "Attempting to screenshot ${url} with ${cookie} using ${scriptfile}"

node ${scriptfile} --cookie ${cookie} --margin-left 0 --margin-right 0 --url ${url} --output_file /tmp/out.pdf && echo "PDF available at /tmp/out.pdf" && sleep 1000