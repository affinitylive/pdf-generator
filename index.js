#!/usr/bin/env node
const puppeteer = require("puppeteer-core");
const argv = require("yargs").argv;
generatePdf();

async function generatePdf() {
	let browser, page_size;
	const incrementalTimeout = [3000, 10000, 300000];
	try {
		browser = await puppeteer.launch({
			headless: true,
			ignoreHTTPSErrors: true,
			args: ['--no-sandbox', '--disable-setuid-sandbox', '--disable-dev-shm-usage'],
			executablePath: '/usr/bin/chromium-browser',
		});
		const page = await browser.newPage();
		if (argv.page_width && argv.page_height) {
			page_size = {
				width: argv.page_width,
				height: argv.page_height,
			};
		}
		else {
			page_size = {
				format: argv.page_size || 'A4',
			};
		}

		if (argv.cookie) {
			await page.setCookie(JSON.parse(argv.cookie));
		}

		for (i = 0; i < 3; i++){
			try {
				await page.goto(argv.url, {waitUntil: 'networkidle0', timeout: incrementalTimeout[i]})
				break;
			} catch (e) {
				if (i == 2) {
					console.log(e);
				}
			}
		}

		await page.pdf({
			path: argv.output_file || 'out.pdf',
			printBackground: true,
			margin: {
				top: argv.margin_top,
				bottom: argv.margin_bottom,
				right: argv.margin_right,
				left: argv.margin_left,
			},
			landscape: !!(argv.orientation && argv.orientation === 'landscape'),
			...page_size
		});
	}
	catch (error) {
		const normalizedError = new Error(error);
		console.error(JSON.stringify({ message: normalizedError.message, stackTrace: normalizedError.stack }));
		throw normalizedError;
	}
	finally {
		if (browser) {
			await browser.close();
		}
	}
}
